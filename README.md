# Typing

Practice typing speed using any file as input. Basic statistics like time,
total characters, accuracy and errors are printed at the end.

Output of `typing -h`:

```
typing v0.1

Usage
typing [options] /path/to/file

Options
-h  print help message
-s  convert every whitespace to a space
```
