#define _XOPEN_SOURCE_EXTENDED
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <curses.h>
#include <locale.h>
#include <wctype.h>

#define VERSION "0.1"
// Max number of chars that are read from a file
#define BUFF_SIZE 3000

void printHelp(void){
    printf("typing v%s\n", VERSION);
    printf("\n");
    puts("Usage");
    puts("typing [options] /path/to/file");
    printf("\n");
    puts("Options");
    puts("-h  print help message");
    puts("-s  convert every whitespace to a space");
}
/*
 * Returns the differens end - start in seconds.
 */
long double diff(struct timespec end, struct timespec start){
   long double result_nanosec = (end.tv_sec - start.tv_sec) * 1000000000.0L;
   result_nanosec += (end.tv_nsec - start.tv_nsec);
   return result_nanosec / 1000000000.0L;
}

int main(int argc, char *argv[]){
    unsigned int numChars = 0;
    /* Number of correct/incorrect input characters */
    int correct = 0, incorrect = 0;
    double accuracy;
    struct timespec start, end;
    wint_t buf[BUFF_SIZE];
    wint_t c;
    FILE *fd;
    long double time;
    int score;
    /* convert whitespaces to a (ascii) space */
    bool wspace_to_space = false;

    setlocale(LC_ALL, "");
    if (argc < 2) {
        printHelp();
        return 0;
    }
    if (strncmp(argv[1], "-h", 2) == 0) {
        printHelp();
        return 0;
    }
    for(int i = 1; i < argc - 1; i++) {
        if (strncmp(argv[i], "-s", 2) == 0) {
            wspace_to_space = true;
        }
        
    }
    // read file
    if((fd = fopen(argv[argc - 1], "r")) == NULL){
        perror("Could not open file for reading");
        return 1;
    }
    c = fgetwc(fd);
    while(c != WEOF && numChars < BUFF_SIZE - 1){
        if (wspace_to_space && iswspace(c)) {
            c = ' ';
        }
        buf[numChars] = c;
        c = fgetwc(fd);
        numChars++;
    }
    if (numChars <= 0){
        perror("Could not read file or file is empty");
        return 1;
    }
    fclose(fd);

    // remove whitespaces at the end 
    buf[numChars] = '\0';
    while (iswspace(buf[numChars - 1])){
        numChars--;
        buf[numChars] = '\0';
    }
    initscr();
    cbreak();	
    noecho();
    curs_set(0);
    timespec_get(&start, TIME_UTC);
    while(buf[correct] != '\0'){
        clear();
        printw("%ls", buf + correct);
        get_wch(&c);
        (c == buf[correct]) ? correct++ : incorrect++;
    }
    timespec_get(&end, TIME_UTC);
    endwin();

    // print stats
    accuracy = (correct - incorrect) / (double) correct;
    time = diff(end, start);
    score = ((correct / time) * ((correct - incorrect * 1.5) / correct)) * 100;
    puts("***");
    printf("Score: %d\n", score);
    printf("%u characters in %.5LF second.\n", correct, time);
    printf("Accuracy %d%%\n", (int) (accuracy * 100.0));
    printf("Incorrect %d\n", incorrect);
    puts("***");
    return 0;
}
