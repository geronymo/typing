BIN_NAME=typing
all:
	gcc -O2 -lncursesw -std=c17 -o build/$(BIN_NAME) src/main.c 
debug:
	gcc -lncurses -g -Wall -Wextra -std=c17 -pedantic -o build/$(BIN_NAME)_debug \
	src/main.c 
